import time
import serial
import random
import numpy as np

#envoie des donnees toutes les 0.2 secondes sur le port serie

with serial.Serial(port='COM3',baudrate=9600, timeout=1) as ser:
    print(ser.name)


    while True:
        time.sleep(0.2)
        nbr=str(np.random.random())
        nbr_bytes=nbr.encode('ascii')
        ser.write(nbr_bytes)


ser.close()
