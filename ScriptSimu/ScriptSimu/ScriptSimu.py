import time
import datetime
import serial
import csv
import numpy as np
from scipy import stats, signal
from scipy.fftpack import fft, ifft, fftfreq
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from cmath import *
import os
import pandas as pd
import statistics
from sklearn import datasets, linear_model

# communication port serie
ser = serial.Serial('COM4', 9600, timeout=0.15)

# initialisation
time_init=datetime.datetime.now()
list_data=[]  #liste contentant les donnees
list_delta=[] #liste contentant les deltas
list_time=[]
test = str(datetime.timedelta(seconds=600.0))
b=True
fe = 5  # frequence 5 Hz

# boucle qui lit les donnees toutes les 5 minutes, calcul de la periode et de la hauteur significative
while b:
    data=ser.readline()     # lecture des donnees du port serie
    data=data.decode('ascii')  # decode les donnees
    time_data=datetime.datetime.now()  # heure des donnees
    delta=str(time_data - time_init)
    
    t = time.mktime(time_data.timetuple())  # convertion du temps en secondes

    list_data.append(data)
    list_delta.append(delta)
    list_time.append(t)
    
    print(list_data)

    if delta > test:
        del list_time[0]     # supprime le premier temps car donnee vide
        del list_data[0]     # premiere donnee vide
        list_y=[]
        list_x=[]

        skipToEnd = False

        for data in list_data:

            # enregistrement des donnees du port serie dans un fichier csv
            #with open('/Users/ETA/Documents/GitHub/vagues/data3/data.csv', 'a') as f:
                #csv.writer(f, delimiter=',', lineterminator='\r').writerow([data])
            #f.close

            y = np.array([data])
            ys = np.squeeze(y)
            ystring = str(ys)
            list_y.append(ystring)
            try:
                y1 = np.asarray([list_y], float)
            except ValueError:
                skipToEnd = True

        
        if skipToEnd==False:
            for t in list_time:
                print(t)
                x = np.array(t)
                list_x.append(x)
                x1 = np.asarray([list_x])

            # Linear regression
            #res = signal.detrend(y1)
            res = y1 - np.mean(y1)

            # Hauteur significative
            hs = np.std(res)*4*10

            # Fourier
            n = res.size
            freq = np.fft.fftfreq(n, d = 0.2)
            fourier = np.fft.fft2(res)
            fourier_sq = np.squeeze(fourier)
            y_fourier = np.abs(fourier_sq)
        
            # Periode
            peak_max = max(y_fourier)
            x_max = freq[y_fourier.argmax()]
            periode = 1/x_max
            periode = abs(periode)

            # Ecriture des resultats dans fichier csv
            hs_str = str(hs)
            periode_str = str(periode)
            time_data_str = str(time_data)
            ligne = time_data_str+" "+hs_str+" "+periode_str

            # Creation d un fichier csv par jour
            todayDate = time.strftime("%d-%m-%y")
            directory = '/Users/ETA/Desktop/Simu/'
            
            if not os.path.exists(directory):
                os.makedirs(directory) 
            file_name = os.path.join(directory, todayDate+'.csv'.format(todayDate))

            with open(file_name,'a') as f:
                csv.writer(f, delimiter=',', lineterminator='\r').writerow([time_data_str, hs_str, periode_str])
            f.close()

        else:
            print("Skip to end was true")
        

        list_data=[]
        list_delta=[]
        list_time=[]
        time_init=datetime.datetime.now()
        delta = 0