import time
import datetime
import serial
import csv
import numpy as np
from scipy import stats, signal
import scipy.signal
from scipy.fftpack import fft, ifft, fftfreq
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from cmath import *
import os
import pandas as pd
import statistics
from sklearn import datasets, linear_model
 
fe = 5  # frequence 5 Hz
T = 1/fe
nbmesure_par_cycle = fe*300
time_data=datetime.datetime.now()

# lecture du fichier csv, regroupe les donnees dans interval de temps 5 minutes
data = pd.read_csv('/Users/ETA/Desktop/newcsv.csv', chunksize = nbmesure_par_cycle)

for chunk in data:
    x = range(0, len(chunk))
    
    y = chunk*10
    y = np.array(y)
    # regression lineaire
    y = y - np.mean(y)

    # Hauteur significative
    hs = np.std(y)*4

    # Fourier
    time = np.linspace(0, len(y), len(y))
    freq = fftfreq(y.size, d = time[1]-time[0])
    fourier = np.fft.fft2(y)
    fourier = np.abs(fourier)

    # Periode
    peak_max = max(fourier)
    x_max = freq[fourier.argmax()]
    x_max = abs(x_max)
    periode = 1/x_max
    periode = abs(periode)
    periode = periode/fe
    print(x_max, peak_max, periode)

    # Ecriture des resultats dans fichier csv
    with open('/Users/ETA/Desktop/simu3.csv', 'a') as f:
        csv.writer(f, delimiter=',', lineterminator='\r').writerow([hs, periode])
    f.close

    
   

