import time
import serial
import random
import csv
import sys

#lecture des donnees sur port serie 3 et les enregistres dans fichier csv

ser = serial.Serial('COM4', 9600, timeout=1)

while True:
    with open('test.csv','a') as f:
        line = ser.readline()
        line_bytes=line.decode('ascii')
        print(line_bytes)
        f.write(line_bytes)
        f.close()
